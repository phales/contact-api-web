
import React, { useEffect, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  container: {
    marginTop: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
}));

export default function UserList() {
  const classes = useStyles();

  const [contacts, setContacts] = useState([]);
  useEffect(() => {
    ContactsGet()
  }, [])
  
  const ContactsGet = () => {
    fetch("http://localhost:5000/identifications/")
      .then(res => res.json())
      .then(
        (result) => {
            setContacts(result)
        }
      )
  }

  const ContactUpdate = id => {
    window.location = '/update/'+id
  }

  const ContactView = id => {
    window.location = '/view/'+id
  }

  const ContactDelete = id => {
    fetch(`http://localhost:5000/identification/${id}`, {
      method: 'DELETE'
    })
    .then(res => res.json())
    .then(
      (result) => {
        alert(result['message'])
        if(result['Status']==='Ok'){
          ContactsGet();
        }
      }
    )
  }

  return (
    <div className={classes.root}>
      <Container className={classes.container} maxWidth="lg">    
        <Paper className={classes.paper}>
          <Box display="flex">
            <Box flexGrow={1}>
              <Typography component="h2" variant="h6" color="primary" gutterBottom>
                USERS
              </Typography>
            </Box>
            <Box>
              <Link to="/create">
                <Button variant="contained" color="primary">
                  CREATE
                </Button>
              </Link>
            </Box>
          </Box>
          <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="right">ID</TableCell>
                <TableCell align="left">FirstName</TableCell>
                <TableCell align="left">LastName</TableCell>
                <TableCell align="left">DOB</TableCell>
                <TableCell align="left">Gender</TableCell>
                <TableCell align="left">Title</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {contacts.map((contact) => (
                <TableRow key={contact.ID}>
                  <TableCell align="right">{contact.id}</TableCell>
                  <TableCell align="left">{contact.FirstName}</TableCell>
                  <TableCell align="left">{contact.LastName}</TableCell>
                  <TableCell align="left">{contact.DOB}</TableCell>
                  <TableCell align="left">{contact.Gender}</TableCell>
                  <TableCell align="left">{contact.Title}</TableCell>
                  <TableCell align="center">
                    <ButtonGroup color="primary" aria-label="outlined primary button group">
                      <Button onClick={() => ContactView(contact.id)}>View</Button>
                      <Button onClick={() =>  ContactUpdate(contact.id)}>Edit</Button>
                      <Button onClick={() => ContactDelete(contact.id)}>Delete</Button>

                    </ButtonGroup>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        </Paper>
      </Container>
    </div>
    
  );
}