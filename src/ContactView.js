
import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { useParams } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function convert(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

export default function ContactUpdate() {
  const classes = useStyles();

  const { id } = useParams();
  useEffect(() => {
    fetch(`http://localhost:5000/identification/${id}`,{ method: 'GET'})
      .then(res => res.json())
      .then((result) => {
          let Identify = result.Identification
          setFname(Identify.FirstName)
          setLname(Identify.LastName)
          setDob(convert(Identify.DOB))
          setGender(Identify.Gender)
          setTitle(Identify.Title)

          let Address = result.Address[0]
          setType(Address.type)
          setNumber(Address.number)
          setStreet(Address.street)
          setUnit(Address.Unit)
          setCity(Address.City)
          setState(Address.State)
          setZipcode(Address.zipcode)
          
          let Comm = result.Communication[0]
          setType2(Comm.type)
          setValue(Comm.value)
          setPref(Comm.preffered)
        }
      )
  }, [id])

  const handleSubmit = event => {
    event.preventDefault();
     window.location.href = '/';
  }

  const [fname, setFname] = useState('');
  const [lname, setLname] = useState('');
  const [dob, setDob] = useState('');
  const [gender, setGender] = useState('');
  const [title, setTitle] = useState('');

  const [typ, setType] = useState('');
  const [number, setNumber] = useState('');
  const [street, setStreet] = useState('');
  const [unit, setUnit] = useState('');
  const [city, setCity] = useState('');
  const [state, setState] = useState('');
  const [zipcode, setZipcode] = useState('');

  const [typ2, setType2] = useState('');
  const [value, setValue] = useState('');
  const [pref, setPref] = useState('');
  return (
    <Container maxWidth="xs">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Contact
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                id="firstName"
                label="First Name"
                value={fname}
                onChange={(e) => setFname(e.target.value)}
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                id="lastName"
                value={lname}
                label="Last Name"
                onChange={(e) => setLname(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                type="date"
                InputLabelProps={{ shrink: true }}
                fullWidth
                disabled = {true}
                id="DOB"
                value={dob}
                label="Date of Birth"
                onChange={(e) => setDob(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                id="Gender"
                value={gender}
                label="Gender"
                onChange={(e) => setGender(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                id="Title"
                value={title}
                label="Title"
                onChange={(e) => setTitle(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                id="type"
                value={typ}
                label="Type of Address (Home, Condo, etc..)"
                onChange={(e) => setType(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                type="number"
                id="number"
                value={number}
                label="Street Number"
                onChange={(e) => setNumber(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                id="street"
                value={street}
                label="Street"
                onChange={(e) => setStreet(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                id="unit"
                value={unit}
                label="Unit"
                onChange={(e) => setUnit(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                id="city"
                value={city}
                label="city"
                onChange={(e) => setCity(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                id="state"
                value={state}
                label="State"
                onChange={(e) => setState(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                type="number"
                id="zipcode"
                value={zipcode}
                label="ZipCode"
                onChange={(e) => setZipcode(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                id="type2"
                value={typ2}
                label="Communication Type (Phone number, Email, Telephone number, etc..."
                onChange={(e) => setType2(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                disabled = {true}
                id="value"
                value={value}
                label="Value* Number/Email etc..."
                onChange={(e) => setValue(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                disabled = {true}
                id="pref"
                value={pref}
                label="Preffered* True or False"
                onChange={(e) => setPref(e.target.value)}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            >
            Back
          </Button>
        </form>
      </div>
    </Container>
  );
}