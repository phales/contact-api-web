import React, { useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function ContactCreate() {
  const classes = useStyles();
  
  const handleSubmit = event => {
    event.preventDefault();
    var data = {
      'FirstName': fname,
      'LastName': lname,
      'DOB': dob,
      'Gender': gender,
      'Title': title,
    }

    var dataAdd = {
      'type': typ,
      'number': number,
      'street': street,
      'Unit': unit,
      'City': city,
      'State': state,
      'zipcode':zipcode 

    }

    var dataComm = {
      'type': typ2,
      'value': value,
      'preffered': pref
    }

    fetch('http://localhost:5000/identification', {
      method: 'POST',
      headers: {
        Accept: 'application/form-data',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }).then(res => res.json())
    .then(result => {
        let id = result['id']
        fetch(`http://localhost:5000/address/${id}`, {
        method: 'POST',
        headers: {
          Accept: 'application/form-data',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(dataAdd),
      }).then(res=>res.json())
      .then(result =>{
        if(result['Status'] === 'Ok'){
          fetch(`http://localhost:5000/communication/${id}`, {
              method: 'POST',
              headers: {
                 Accept: 'application/form-data',
                 'Content-Type': 'application/json',
               },
               body: JSON.stringify(dataComm),
             }).then(res=>res.json())
             .then(result => {
              if(result['Status'] === 'Ok'){
                window.location.href = '/'
              }
             })
        }
      })
      // let comm = 
       /*&& comm['Status'] === 'Ok'*/
      
    })
  }

  const [fname, setFname] = useState('');
  const [lname, setLname] = useState('');
  const [dob, setDob] = useState('');
  const [gender, setGender] = useState('');
  const [title, setTitle] = useState('');

  const [typ, setType] = useState('');
  const [number, setNumber] = useState('');
  const [street, setStreet] = useState('');
  const [unit, setUnit] = useState('');
  const [city, setCity] = useState('');
  const [state, setState] = useState('');
  const [zipcode, setZipcode] = useState('');

  const [typ2, setType2] = useState('');
  const [value, setValue] = useState('');
  const [pref, setPref] = useState('');
  return (
    <Container maxWidth="xs">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Contact
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="First Name"
                onChange={(e) => setFname(e.target.value)}
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                onChange={(e) => setLname(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                type="date"
                InputLabelProps={{ shrink: true }}
                fullWidth
                id="DOB"
                label="Date of Birth"
                onChange={(e) => setDob(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="Gender"
                label="Gender"
                onChange={(e) => setGender(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="Title"
                label="Title"
                onChange={(e) => setTitle(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="type"
                label="Type of Address (Home, Condo, etc..)"
                onChange={(e) => setType(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                type="number"
                id="number"
                label="Street Number"
                onChange={(e) => setNumber(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="street"
                label="Street"
                onChange={(e) => setStreet(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="unit"
                label="Unit"
                onChange={(e) => setUnit(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="city"
                label="city"
                onChange={(e) => setCity(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="state"
                label="State"
                onChange={(e) => setState(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                type="number"
                id="zipcode"
                label="ZipCode"
                onChange={(e) => setZipcode(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="type2"
                label="Communication Type (Phone number, Email, Telephone number, etc..."
                onChange={(e) => setType2(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="value"
                label="Value* Number/Email etc..."
                onChange={(e) => setValue(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                id="pref"
                label="Preffered* True or False"
                onChange={(e) => setPref(e.target.value)}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            >
            Create
          </Button>
        </form>
      </div>
    </Container>
  );
}