import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Navbar from './Navbar'
import Contacts from './Contacts'
import ContactCreate from './ContactCreate'
import ContactUpdate from './ContactUpdate'
import ContactView from './ContactView'
export default function App() {
  return (
    <Router>
      <div>
      <Navbar />
        <Routes>
          <Route exact path='/' element={<Contacts/>}/>
          <Route exact path='/create' element={<ContactCreate/>}/>
          <Route exact path='/update/:id'  element={<ContactUpdate/>}/>
          <Route exact path='/view/:id'  element={<ContactView/>}/>
        </Routes>
      </div>
    </Router>
  ); 
}

// import './App.css';
// //import 'semantic-ui-css/semantic.min.css'

// function App() {
//   return (
//     <div className="main">
//         <h2 className="main-header"> Contact API </h2>
//     </div>
//   );
// }

// export default App;
